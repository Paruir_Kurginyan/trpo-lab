﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRPO_Lab3.WebApp.Models
{
    public class MathViewModel
    {
        public int r1 { set; get; }
        public int r2 { set; get; }
        public int h { set; get; }
        public string result { set; get; }

    }
}
