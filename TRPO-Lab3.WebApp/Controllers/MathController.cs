﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TRPO_Lab3.Lib;
using TRPO_Lab3.WebApp.Models;

namespace TRPO_Lab3.WebApp.Controllers
{
    public class MathController : Controller
    {
        public IActionResult Index(int h, int r1, int r2)
        {
            var model = new MathViewModel();
            model.r1 = r1;
            model.r2 = r2;
            model.h = h;

            try
            {
                model.result = (new Ball().Ball_Layer(model.r1, model.r2, model.h)).ToString();
            }
            catch (ArgumentException ex)
            {
                model.result = ex.Message;
            }
            return View(model); ;
        }

    }
}
