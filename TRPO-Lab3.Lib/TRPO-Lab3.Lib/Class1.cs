﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Ball
    {
        public double Ball_Layer(int r1, int r2, int h)
        {
            if (r1 <= 0 | r2 <= 0 | h <= 0)
                throw new ArgumentException("Incorrect values!");
            return Math.Round(Math.PI * Math.Pow(h, 3) / 6 + Math.PI * (r1 * r1 + r2 * r2) * h / 2, 2);
        }
    }
}
