using NUnit.Framework;
using System;

namespace TRPO_Lab3.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {

            const int r1 = 4;
            const int r2 = 5;
            const int h = 1;
            const double expected = 64.93;

            var result = new Lib.Ball().Ball_Layer(r1, r2, h);

            Assert.That(result, Is.EqualTo(expected));
        }
        [Test]
        public void Test2()
        {

            const int r1 = 0;
            const int r2 = 2;
            const int h = 3;

            TestDelegate action = () => new Lib.Ball().Ball_Layer(r1, r2, h);
            Assert.Throws<ArgumentException>(action);

        }
    }
}