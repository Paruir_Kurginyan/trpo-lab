﻿using System;

namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите радиус первого основания шарового пояса r1: ");
            int r1 = int.Parse(Console.ReadLine());
            Console.Write("Введите радиус второго основания шарового пояса r2: ");
            int r2 = int.Parse(Console.ReadLine());
            Console.Write("Введите высоту шарового слоя h: ");
            int h = int.Parse(Console.ReadLine());

            try
            {
                var action = new Lib.Ball().Ball_Layer(r1, r2, h);
                Console.WriteLine($"Объем шарового слоя равен: {action}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
